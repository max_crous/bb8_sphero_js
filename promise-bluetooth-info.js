"use strict";

var sphero = require("sphero");
var orb = sphero("c0:d0:ac:38:8b:f4");

orb.connect().then(function() {
  return orb.color("FF00FF");
}).then(function() {
  return orb.getBluetoothInfo();
}).then(function(data) {
  console.log("bluetoothInfo:");
  console.log("  name:", data.name);
  console.log("  btAddress:", data.btAddress);
  console.log("  separator:", data.separator);
  console.log("  colors:", data.colors);
}).catch(function(err) {
  console.error("err:", err);
});
