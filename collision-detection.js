"use strict";

var sphero = require("sphero");
var orb = sphero("c0:d0:ac:38:8b:f4");


orb.connect(function() {
  orb.detectCollisions();
  orb.color("green");

  orb.on("collision", function(data) {
    console.log("collision detected");
    console.log("  data:", data);

    orb.color("red");

    setTimeout(function() {
      orb.color("green");
    }, 1000);
  });

  orb.roll(155, 0);
});
