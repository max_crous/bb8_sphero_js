"use strict";

var sphero = require("sphero");
var orb = sphero("c0:d0:ac:38:8b:f4");

var PythonShell = require('python-shell');
var distance = 40;
var string = "32.3";
var direction = 0;
var speed = 250;

// aantal keren dat er een beweging naar links/rechts uitgevoerd is
var angle_change = 0;

// 0 == recht vooruit, 1 == links of rechts
var orientation = 0;

// 1 ==  rechts, -1 == links
var side = 1;

// obstakel aan de voorkant
var front_collision = false;

orb.connect(function() {

  var updating = false;

  // Dit zijn standaard values
  console.log("standard values worden gezet");
  orb.roll(0,0);
  orb.color({ red: 255, green: 255, blue: 255 });

  function pythini(){
                  if (updating) { return; }
                  PythonShell.run('gpio.py', function(err, results) {

                  if (err) throw err;
                  distance = results[0];
                  updating = true;
                });
    } setInterval(pythini, 400);

    function update(){
      if (!updating) { return; }
      console.log("update wordt aangeroepen");
      console.log(distance);

      console.log("orientation: ");
      console.log(orientation);

      console.log("fc: ");
      console.log(front_collision);

      // obstakel in de vooruit richting
      if(orientation == 0 && distance < 50) {
	front_collision = true;
	orientation = 1;
      }

      // obstakel in de linker of rechter richting
      if(orientation == 1 && distance < 50 && angle_change == 1) {side = 0 - side;}

      // vooruit rijden	
      if(!front_collision && angle_change < 1){
	orientation = 0;
        orb.roll(speed, 0);
        orb.color({ red: 0, green: 255, blue: 0 });
      }

      // Naar links rijden voor side == -1 en naar rechts rijden voor side == 1
      else {
        orb.color({ red: 255, green: 0, blue: 0 });
        orb.roll(speed, (side * 110));
        angle_change += 1;

        if (angle_change >= 2) {
		angle_change = 0;
		front_collision = false;
	}

        console.log("angle change:");
        console.log(angle_change);

      }

      updating = false;
      return;
    } setInterval(update, 400);

});
