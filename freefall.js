"use strict";

var sphero = require("sphero");
var orb = sphero("c0:d0:ac:38:8b:f4", {timeout: 200});

orb.connect(function() {
  orb.detectFreefall();

  orb.on("freefall", function(data) {
    console.log("freefall detected");
    console.log("  data:", data);
  });

  orb.on("landed", function(data) {
    console.log("landing detected");
    console.log("  data:", data);
  });

});
