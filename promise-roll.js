"use strict";

var sphero = require("sphero");
var orb = sphero("c0:d0:ac:38:8b:f4");

var PythonShell = require('python-shell');
var distance = 40;
var string = "32.3";
var direction = 0;
var speed = 250;
var angle_change = 0;

orb.connect(function() {

  var updating = false;

  // Dit zijn standaard values
  console.log("standard values worden gezet");
  orb.roll(0,0);
  orb.color({ red: 255, green: 255, blue: 255 });

  function pythini(){
                  if (updating) { return; }
                  PythonShell.run('gpio.py', function(err, results) {

                  if (err) throw err;
                  distance = results[0];
                  updating = true;
                });
    } setInterval(pythini, 400);

    function update(){
      if (!updating) { return; }
      console.log("update wordt aangeroepen");
      console.log(distance);
      console.log("Rol moet zijn gerold");

      if( distance > 50  && angle_change < 1){
        orb.roll(speed,direction);
        orb.color({ red: 0, green: 255, blue: 0 });
      }

      // draai naar rechts
      else {
        // direction += 135;
        orb.color({ red: 255, green: 0, blue: 0 });
        orb.roll(speed, 110);
        angle_change += 1;
        if (angle_change >= 2) {angle_change = 0}
        console.log("angle change:");
        console.log(angle_change);
      }

      updating = false;
      return;
    } setInterval(update, 400);

});
