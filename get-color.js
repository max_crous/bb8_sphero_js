"use strict";

var sphero = require("sphero");
var orb = sphero("c0:d0:ac:38:8b:f4");

orb.connect(function() {
  // sets color to the provided r/g/b values
  orb.getColor(function(err, data) {
    if (err) {
      console.log(err);
    } else {
      console.log("Color is:", data.color);
    }
  });
});
