
"use strict";

/* eslint no-use-before-define: 0 */
/* eslint no-process-exit: 0 */

var sphero = require("sphero");
var orb = sphero("c0:d0:ac:38:8b:f4");


orb.connect().then(function() {

    orb.sleep(10, 0, 0, function(err, data) {
      console.log(err || "data: " + data);
    });
});
