
"use strict";

/* eslint no-use-before-define: 0 */
/* eslint no-process-exit: 0 */

var sphero = require("sphero");
var orb = sphero("c0:d0:ac:38:8b:f4");


orb.connect().then(function() {

      orb.getPowerState(function(err, data) {
      if (err) {
        console.log("error: ", err);
      } else {
        console.log("data:");
        console.log("  recVer:", data.recVer);
        console.log("  batteryState:", data.batteryState);
        console.log("  batteryVoltage:", data.batteryVoltage);
        console.log("  chargeCount:", data.chargeCount);
        console.log("  secondsSinceCharge:", data.secondsSinceCharge);
      }
    });

});
