"use strict";

var sphero = require("sphero");
var orb = sphero("c0:d0:ac:38:8b:f4");

var PythonShell = require('python-shell');
var distance = 40;
var string = "32.3";
var direction = 0;
var speed = 200;
var angle_change = 0;
var req_dist = 40;
var original_direction = 0;
var left = false;
var right = true;

orb.connect(function() {

  var updating = false;

  // Dit zijn standaard values
  console.log("standard values worden gezet");
  orb.roll(0,0);
  orb.color({ red: 255, green: 255, blue: 255 });

  function pythini(){
                  if (updating) { return; }
                  PythonShell.run('gpio.py', function(err, results) {

                  if (err) throw err;
                  distance = results[0];
                  updating = true;
                });
    } setInterval(pythini, 200);

    function update(){
      if (!updating) { return; }
      console.log("update wordt aangeroepen");
      console.log(distance);
      console.log("Rol moet zijn gerold");

      if( distance > req_dist  && angle_change == 0){
        orb.roll(speed,direction);
        orb.color({ red: 0, green: 255, blue: 0 });
      }

      else if( distance < req_dist  && angle_change == 0){
        angle_change = 5;
        orb.color({ red: 255, green: 0, blue: 0 });

        if(left){
          direction += 290;
          right = true;
          left = false;
        }

        else if(right)
        {
          direction += 120;
          right = false;
          left = true;
        }

        orb.roll(speed,direction);
      }

      if (angle_change == 1) {
        direction = original_direction;
        angle_change -= 1;
      }

      else if (angle_change > 0) {
        angle_change -= 1;
      }

      console.log("angle change ");
      console.log(angle_change);

      updating = false;
      return;
    } setInterval(update, 200);

});
