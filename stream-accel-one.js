"use strict";

var sphero = require("sphero");
var orb = sphero("c0:d0:ac:38:8b:f4");


orb.connect(function() {
  orb.streamAccelOne();

  orb.on("accelOne", function(data) {
    console.log("accelOne:");
    console.log("  sensor:", data.accelOne.sensor);
    console.log("  range:", data.accelOne.range);
    console.log("  units:", data.accelOne.units);
    console.log("  value:", data.accelOne.value[0]);
  });

  orb.roll(180, 0);
});
