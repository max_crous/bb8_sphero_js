"use strict";

var sphero = require("sphero");
var orb = sphero("c0:d0:ac:38:8b:f4");


orb.connect(function() {
  console.log("::START CALIBRATION::");
  orb.startCalibration();
  setTimeout(function() {
    console.log("::FINISH CALIBRATION::");
    orb.finishCalibration();
  }, 5000);
});
