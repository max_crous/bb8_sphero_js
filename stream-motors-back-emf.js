"use strict";

var sphero = require("sphero");
var orb = sphero("c0:d0:ac:38:8b:f4");

orb.connect(function() {
  orb.streamMotorsBackEmf();

  orb.on("motorsBackEmf", function(data) {
    console.log("motorsBackEmf:");
    console.log("  sensor:", data.rMotorBackEmf.sensor);
    console.log("    range:", data.rMotorBackEmf.range);
    console.log("    units:", data.rMotorBackEmf.units);
    console.log("    value:", data.rMotorBackEmf.value[0]);

    console.log("  sensor:", data.lMotorBackEmf.sensor);
    console.log("    range:", data.lMotorBackEmf.range);
    console.log("    units:", data.lMotorBackEmf.units);
    console.log("    value:", data.lMotorBackEmf.value[0]);
  });

  orb.roll(180, 0);
});
