"use strict";

var sphero = require("sphero");
var orb = sphero("c0:d0:ac:38:8b:f4");

orb.connect().then(orb.detectCollisions).then(function() {
  return orb.color("green");
}).then(function() {
  return orb.roll(155, 0);
});

orb.on("collision", function(data) {
  console.log("collision detected");
  console.log("  data:", data);

  orb.color("red").delay(100).then(function() {
    return orb.color("green");
  });
});
